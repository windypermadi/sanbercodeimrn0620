//if - else
var nama = "windy"
var peran = "guard"

console.log("-- if else --")
if ( nama == "" && peran == "") {
    console.log("Nama harus diisi!")
} else if ( nama != "" && peran == "" ){
	console.log("Halo ",nama, "Pilih peranmu untuk memulai game!")
} else {
	console.log("Selamat datang di Dunia Werewolf, " ,nama)
		if ( peran == "Penyihir" || peran == "penyihir" ) {
			console.log("Halo Penyihir ",nama, " kamu dapat melihat siapa yang menjadi werewolf!")
		} else if ( peran == "Guard" || peran == "guard" ) {
			console.log("Halo Guard ",nama, " kamu akan membantu melindungi temanmu dari serangan werewolf.")
		} else if ( peran == "Werewolf" || peran == "werewolf" ){
			console.log("Halo Werewolf ",nama, " Kamu akan memakan mangsa setiap malam!")
		}
}

//switch case
var tanggal = 12;
var bulan = 12; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1996; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
var bulan2;

switch(bulan) {
  case 1:    { 	bulan2 = 'Januari'; break; }
  case 2:    { bulan2 = 'Februari'; break; }
  case 3:    { bulan2 = 'Maret'; break; }
  case 4:    { bulan2 = 'April'; break; }
  case 5:    { bulan2 = 'Mei'; break; }
  case 6:    { bulan2 = 'Juni'; break; }
  case 7:    { bulan2 = 'Juli'; break; }
  case 8:    { bulan2 = 'Agustus'; break; }
  case 9:    { bulan2 = 'September'; break; }
  case 10:   { bulan2 = 'Oktober'; break; }
  case 11:   { bulan2 = 'November'; break; }
  case 12:   { bulan2 = 'Desember'; break; }
  default:   { bulan2 = '-'; }}

	console.log("-- switch case --")
  	console.log(tanggal, bulan2, tahun);