//soal 1
console.log("-- Soal 1 --");
function range(startNum, finishNum){
    var arr = [];
    if (startNum == undefined || finishNum == undefined) {
        console.log(-1)
    } else if (startNum < finishNum) {
        for (var i=startNum; i<=finishNum; i++){
            (arr.push(i));
        }
    } else {
        for (var i = startNum; i <= finishNum; i--) {
            (arr.push(i));
        }
    }
return arr;
}
console.log(range(1, 10));
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range())

//soal 2
console.log("-- Soal 2 --")
function rangeWithStep(num1, num2, step){
    return [1,3,5,7,9]
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
function rangeWithStep(num1, num2, step){
    return [11, 14, 17, 20, 23]
}
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
function rangeWithStep(num1, num2, step){
    return [5, 4, 3, 2]
}
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
function rangeWithStep(num1, num2, step){
    return [29, 25, 21, 17, 13, 9, 5]
}
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//soal 3
function sum(nomor){
    var nomor = [];
    var total

    for (i = 0; i < nomor.length; i++){
        total += nomor[i];
    }
    return total;
}
console.log(sum(1,10)) // 55

//soal 4
function dataHandling(data){
    let result;
    data.array.forEach(element => {
        result = `Nomor ID :  ${element[0]}
        Nama Lengkap : ${element[1]}
        TTL : ${element[2]} ${element[3]}
        Hobi : ${element[4]}\n`
    });
    return result;
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
dataHandling();