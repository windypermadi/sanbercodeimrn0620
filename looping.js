//soal 1
console.log('-- Soal 1 --')
var flag = 2;
var flag2 = 20;
console.log('LOOPING PERTAMA');
while(flag <= 20) { 
  console.log(flag, 'I love coding'); 
  flag+=2;
}
console.log('LOOPING KEDUA');
while(flag2 >= 2) { 
    console.log(flag2, 'I will become a mobile developer');
    flag2-=2; 
  }

//soal 2
console.log('-- Soal 2 --')
for( var angka = 1; angka <= 20; angka++){
    if((angka%2)==1){
        if (angka%3===0){
            console.log(angka, 'I love coding');
        } else {
            console.log(angka, 'Santai');
        }
      
    }
    else if ((angka%2)===0) {
      console.log(angka, 'Berkualitas');
    }
}

//soal 3
var s = '';
for ( var i = 0; i < 4; i++ ){
    for ( var j = 0; j < 8; j++ ){
        s += '#';
    }
    s += '\n';
}
console.log('-- Soal 3 --')
console.log(s);

//soal 4
var x = '';
for ( var i = 0; i < 7; i++ ){
    for ( var j = 0; j <= i; j++ ){
        x += '#';
    }
    x += '\n';
}
console.log('-- Soal 4 --')
console.log(x);

//soal 5 
var catur = '';
for ( var a = 0; a < 8; a++){
    for (var b = 0; b < 8; b++){
        if ((b%2 == 0 && a%2 == 0) || (b%2 == 1 && a%2 == 1) ){
            catur += ' ';
        } else {
            catur += '#';
        }
    }
    catur += '\n';
}
console.log('-- Soal 5 --')
console.log(catur);